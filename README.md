# rpiggio_sim  ![Arppy - Project Mascot](images/arppy_logo_s.png) 


## Overview
*_RPiggio_* is a General Purpose Input/Output (GPIO) RaspberryPi "simulator" for Python development

### Caveat
This *_RPiggio_* is an small exploratory project. Documentation is minimal currently.

### Who Would Use This 
If you're writing some code for an RPi board, but probably want to draft it on your desktop machine, in your favourite IDE, and test it before you send it to your Raspberry Pi hardware.

 *_RPiggio_* will help you out!  *RPiggio* simulates the behavior of the RPi.GPIO Python library on the RaspberryPi Hardware.

    This software lets you write Python code for RaspberryPi hardware, 
    but thoroughly test and debug it before you run it on your RPi. 
    Deal with your syntax and algorithmic mistakes without the hassles 
    of file transfers. 
    This library lets you fix bugs in your favourite editor/IDE without 
    getting into tedious multiple transfer steps, and avoid the cumbersome 
    debugging environment of the target Raspberry Pi hardware.
    
###License ###

**Creative Commons - Attribution**

You are free to use this code but need to retain this license statement and 
link to the license terms. 
You need to also acknowledge the author in any use or derivative you create.  
** See license terms to which you are agreeing here: 
[Creative Commons license terms](https://creativecommons.org/licenses/by/4.0/)

###Simplicity – Go Elsewhere for Features###

There are other very rich RPi GPIO emulator/simulators around - much fancier than *_RPiggio_*.
The benefit of *_RPiggio_* is that it's very light weight and easy to understand. That's about it at this point.

You can use either a Python 2.7 or Python 3.x version. If you want more functionality, you can find some options online that have features like graphical display of the GPIOs etc. 

The *_RPiggio_* library was created from scratch by the author, purely to allow the coding of 
RPi commands on a desktop/laptop (eg MacOSX) machine. It should work fine on any platform where you can set up a local library.

#### Arppy – the Project Mascot ####

Every project needs a mascot.  Arppy was found in the open clipart mud on Pixbay and taken to live in my repository.

# To Use RPiggio:
Simply put the rpiggio_sim.py file in the same directory as your new Python code and evoke it with an import:

    from RPiggio import *                     # add the simulator objects etc
    GPIO = RPi_GPIO_Surrogate()               # Standin for the GPIO init
    
    SW1_PIN = 15                              # choose a GPIO pin to attach a switch to
    GPIO.PIN_SIM[ SW1_PIN ] = HI              # set an initial state

You can also just add it to a directory of existing local libaries if you have that set up.  Remember that a local library needs the dummy file "\__init__.py"  within it. You can then import from it.  

RPiggio offers a surrogate GPIO object that you can use to proceed with normal GPIO cmds even though you're not running it on RPi hardware. Commands like:

    GPIO.setup(SW3_PIN, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)       # Set PullDown
    sleep(2)
    print "Our pin sees: " + str(GPIO.input(SW1_PIN))
    
A useful approach to make your code easily transportable between your development machine and the target hardware (ie an RPi board) is to conditionally load the simulator. So if your desktop is detected at runtime load RPiggio, otherwise use actual RPi commands from the target's real GPIO library on the RPi hardware. 
Use the platform library in Python:

    import platform
    thisBox = platform.system()
     
    SW1_PIN = 15                                  # choose a GPIO pin to attach a switch to
    
    if thisBox == "Darwin":                       # Check for dev enviro - ie MacOSX
        from RPiggio import *                     # add the objects etc
        GPIO = RPi_GPIO_Surrogate()               # Standin for the GPIO init
        GPIO.PIN_SIM[ SW1_PIN ] = HI              # set an initial state for simulator on pin 15
        
    elif thisBox == "Linux":                      # Check for actual Raspberry Pi board
        import RPi.GPIO as GPIO                   # Load actual GPIO lib on the target RPi
    
    # then proceed as per normal RPi code...
    GPIO.setup(SW3_PIN, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)       # Set PullDown
    sleep(2)
    print "Our pin sees: " + str(GPIO.input(SW1_PIN))
    
The result is that the same code can be run on the RPi without any changes and you can 
rapidly debug your code on your Mac.  (If you're on Windows, you just check the documentation 
for `platform()` and detect that.) 

You can even do interrupts with RPiggio. The process is to fire a probablistic event, for a thread-based interrupt that calls your designated interrupt code the same way the real library sets that up. 
Some sample code for you ... 

Basically a proper interrupt setup uses a setup for your chosen pin as follows:
    
    GPIO.add_event_detect( SW1_PIN, GPIO.RISING, callback=interruptRoutine)     # designate an interrupt routine

When an interrupt fires, it calls your designated interrupt routine. 

    def interruptRoutine( timeStamp):
        ''' Routine called upon interrupt event'''
        print " INTERRUPT called "

Thus a pin voltage change initiates the interrupt, and evokes your designated code. 

RPiggio causes the same behaviour with an interrupt not based on a pin-voltage change but rather based on some randomness you tweak to your needs in the library. Just adjust the `minT` and `maxT` values in the `run(self)` block to make an interrupt more or less likely, based on your testing. 

You can make an interrupt very likely or impossible so that your code can run and test its functionality thoroughly on your desktop before you try it on an RPi board.

Some nice things can be done with the python Queue library to process threaded events. Explore that for further feature possibilities.
_More to come on this later_